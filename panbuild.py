#! /usr/bin/python3

import sys
from pathlib import Path
import yaml
from contextlib import contextmanager

import pandoc
from pandoc.types import *

class Target:
    pass

def istarget(el):
    return isinstance(el, Link) and el[1][0][0][0] == "!"

class Build:
    def __init__(self, config):
        self._config    = config
        self._metadata  = config["variables"]
        self._build     = config["build"]
        self._rules     = config["build"]["rules"]
        self._builddir  = Path(config["build"]["directory"])
        self._buildfile = Path("build.ninja")

        self._doc = pandoc.read(file=config["build"]["index"])

    @contextmanager
    def open(self):
        self._output_file = open(self._buildfile, "w")
        try:
            yield self._output_file
        finally:
            self._output_file.close()

    def print(self, content=""):
        print(content, file=self._output_file)

    def write_metadata(self):
        with open(".metadata.yml", "w") as f:
            yaml.dump({"variables": self._metadata}, f)

    def output_variables(self):
        for key, value in self._config["build"].items():
            if type(value) in [list, dict]:
                continue
            if key in ["header", "footer"]:
                continue
            self.print(f"{key}={value}")
        self.print()

    def output_header(self):
        if self._config["build"]["header"]:
            self.print(self._config["build"]["header"])

    def output_rules(self):
        for name in self._rules:
            command = self._rules[name]["command"]
            self.print(f"""rule {name}
  command = {command}""")
        self.print()

    def output_index(self):
        pandoc.write(self._doc, ".index.native", format="native", options=["--standalone"])

        filename = self._builddir / "index.html"

        self.print()
        self.print(f"""build {filename}: html .index.native
default {filename}""")

    def _process_index(self):
        for el in pandoc.iter(self._doc):
            if istarget(el):
                self._process_link(el)

    def output_targets(self):
        self._process_index()

    def output_footer(self):
        if self._config["build"]["footer"]:
            self. print()
            self.print(self._config["build"]["footer"])

    def _process_link(self, link):
        config = self._config

        inline = link[1]
        first  = inline[0][0]
    
        rulename = inline[0][0][1:]
        source = link[2][0]
    
        target = self._build_target(rulename, source)
    
        # Rewrite link target
        link[2] = (str(target.relative_to(self._builddir)), link[2][1])

        # Rewrite link name
        if len(inline) > 1:
            inline.pop(0)
            while isinstance(inline[0], Space):
                inline.pop(0)
        else:
            inline[0] = Str(first[1:])

    def _build_target(self, rule, source):
        config = self._config

        if "extension" in config["build"]["rules"][rule]:
            target = Path(source).stem + "." + config["build"]["rules"][rule]["extension"]
        else:
            target = str(source)

        if config["build"]["prefix"]:
            prefix = dollar_expansion(config["build"]["prefix"], config["build"])
            target = prefix + "-" + target
        target = self._builddir / target

        self.print(f"build {target}: {rule} {source}")
        self.print(f"default {target}")
        return target

def dollar_expansion(expr, variables):
    """ Very simple and probably broken
    """
    if expr.startswith("$"):
        return variables[expr[1:]]
    else:
        return expr

def main():
    try:
        with open("Panbuild.yml") as f:
            config = yaml.safe_load(f)
    except FileNotFoundError:
        print("No Panbuild.yml file found, exiting", file=sys.stderr)
        sys.exit(1)
    except PermissionError:
        print("Unable to read Panbuild.yml file, exiting", file=sys.stderr)
        sys.exit(1)

    build = Build(config)
    build.write_metadata()
    with build.open():
        build.output_variables()
        build.output_header()
        build.output_rules()
        build.output_targets()
        build.output_index()
        build.output_footer()

if __name__ == "__main__":
    main()
